#define _POSIX_C_SOURCE 2
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
  YOUR WORK:

  You will complete the function setup_main_stack below. Missing code
  and calculations are expressed with a questionmark (?). The
  assignment is mostly about doing the correct math. To do that math
  you will need to learn or understand:
  
  - How the stack is used during program execution
  - How the stack is expected to look like at start of a C-program
  - What a pointer variable is, how it is used
  - How a C-array (pointer to many) is laid-out in memory 
  - How pointer arithmetic behaves
  - How to cast from one pointer type to another
  - How to use printf to print good debug information
  - How to use a C-struct, both object and pointer to

  Some manual-pages of interest:

  man -s3 printf
  man -s3 strlen
  man -s3 strncpy
  man -s3 strtok_r

  The prototype for above functions:
  
  int printf(const char *restrict format, ...);
  size_t strlen(const char *s);
  size_t strncpy(char *dst, const char *src, size_t dstsize);
  char *strtok_r(char *s1, const char *s2, char **lasts);
  
  The above functions exist in Pintos code. Note the Pintos use
  'strlcpy' instead of 'strncpy'. If 'dst' is large enough they behave
  identical. You can find Pintos implementations in
  /home/TDIU16/labs/skel/pintos/src/lib, see string.c and stdio.c. It
  is strongly recommendable to look at the strtok_r examples given in
  string.c

  About *restrict: http://en.wikipedia.org/wiki/Pointer_alias


  Recommended compile command:
  
  gcc -m32 -Wall -Wextra -std=gnu99 -pedantic -g setup-argv.c
  
*/
//#error Read comments above, then remove this line.

#define true 1
#define false 0

typedef int bool;

/* "struct main_args" represent the stack as it must look when
 * entering main. The only issue: argv must point somewhere...
 *
 * The members of this structure is is directly related to the
 * arguments of main in a C-program, of course.
 *
 * int main(int argc, char* argv[]);
 *
 * (char** argv is a more generic form of char* argv[])
 *
 * The function pointer is normally handled by the compiler
 * automatically, and determine the address at where a function shall
 * continue when it returns. The main function does not use it, as the
 * operating system arrange for the program to stop execution when
 * main is finished.
 */
struct main_args
{
  /* Hint: When try to interpret C-declarations, read from right to
   * left! It is often easier to get the correct interpretation,
   * altough it does not always work. */

  /* Variable "ret" that stores address (*ret) to a function taking no
   * parameters (void) and returning nothing. */
  void (*ret)(void);

  /* Just a normal integer. */
  int argc;
  
  /* Variable "argv" that stores address to an address storing char.
   * That is: argv is a pointer to char*
   */
  char** argv;
};

/* A function that dumps 'size' bytes of memory starting at 'ptr'
 * it will dump the higher adress first letting the stack grow down.
 */
void dump(void* ptr, int size)
{
  int i;
  
  printf("Adress  \thex-data \tchar-data\n");
  
  for (i = size - 1; i >= 0; --i)
  {
    void** adr = (void**)((unsigned)ptr + i);
    unsigned char* byte = (unsigned char*)((unsigned)ptr + i);

    printf("%08x\t", (unsigned)ptr + i); /* address */
      
    if ((i % 4) == 0)
      /* seems we're actually forbidden to read unaligned adresses */
      printf("%08x\t", (unsigned)*adr); /* content interpreted as address */
    else
      printf("        \t"); /* fill */
        
    if(*byte >= 32 && *byte < 127)
      printf("%c\n", *byte); /* content interpreted as character */
    else
      printf("\\%o\n", *byte);
    
    if ((i % 4) == 0)
      printf("------------------------------------------------\n");
  }
}

/* Read one line of input ...
 */
void custom_getline(char buf[], int size)
{
  int i;
  for (i = 0; i < (size - 1); ++i)
  {
    buf[i] = getchar();
    if (buf[i] == '\n')
      break;
  }
  buf[i] = '\0';
}



/* Return true if 'c' is fount in the c-string 'd'
 * NOTE: 'd' must be a '\0'-terminated c-string
 */
bool exists_in(char c, const char* d)
{
  int i = 0;
  while (d[i] != '\0' && d[i] != c)
    ++i;
  return (d[i] == c);
}

/* Return the number of words in 'buf'. A word is defined as a
 * sequence of characters not containing any of the characters in
 * 'delimeters'.
 * NOTE: arguments must be '\0'-terminated c-strings
 */

int count_args(const char* buf, const char* delimeters)
{
  int i = 0;
  bool prev_was_delim;
  bool cur_is_delim = true;
  int argc = 0;

  while (buf[i] != '\0')
  {
    prev_was_delim = cur_is_delim;
    cur_is_delim = exists_in(buf[i], delimeters);
    argc += (prev_was_delim && !cur_is_delim);
    ++i;
  }
  return argc;
}

/* Replace calls to STACK_DEBUG with calls to printf. All such calls
 * easily removed later by replacing with nothing. */
#define STACK_DEBUG(...) printf(__VA_ARGS__)

void* setup_main_stack(const char* command_line, void* stack_top)
{

  struct main_args* esp;
  int argc;
  int total_size;
  int line_size;
  char* cmd_line_on_stack;


  line_size = strlen(command_line) + 1;
  STACK_DEBUG("# line_size = %d\n", line_size);
  while (line_size % 4 != 0) {
	line_size++;
  }
  STACK_DEBUG("# line_size (aligned) = %d\n", line_size);

  argc = count_args(command_line, " ") ;
  STACK_DEBUG("# argc = %d\n", argc);
 
  int lines = line_size/4; 		          //lines = the number of lines our input(command_line) will take up in the stack
  total_size = lines * 4 + (argc+2) * 4 +2 *4;    //number of lines* size + number of arguments * size + other registers * size


  STACK_DEBUG("# total_size = %d\n", total_size);

  unsigned esp_value =  (stack_top - total_size);
  unsigned argv_value =  esp_value + 3*4;		             //where argv starts in stack
  unsigned cmd_line_on_stack_value = argv_value + (argc+1)*4;   //where cmd_line starts in stack

  esp = (struct main_args*)esp_value;
  esp->ret = NULL;
  esp->argc = argc;
  esp->argv = (char**)argv_value;
  cmd_line_on_stack = (char*)cmd_line_on_stack_value;
  
    unsigned word_count=0;
    esp->argv[word_count++] = &cmd_line_on_stack[0];
    for(int c=0; c<strlen(command_line); c++) {
	char character = command_line[c];
	if (character == ' ') { 			           //space = new word will begin next iteration
		cmd_line_on_stack[c] = '\0'; 			   // \0 = end of current word


								   //esp->argv[index] will point to the first char of every word
		esp->argv[word_count++] = &cmd_line_on_stack[c+1]; //the next character (c+1), will be the first char of the next word.
	} else {
	cmd_line_on_stack[c] = character;			   //writes modified input to the stack
        }
    }

  return esp; /* the new stack top */
}

/* The C way to do constants ... */
#define LINE_SIZE 1024

int main()
{
  struct main_args* esp;
  char line[LINE_SIZE];
  void* simulated_stack = malloc(4096);
  void* simulated_stack_top = (void*)((unsigned)simulated_stack + 4096);
  int i;
  
  /* read one line of input, this will be the command-line */
  printf("Mata in en mening: ");
  custom_getline(line, LINE_SIZE);
  
  /* put initial content on our simulated stack */
  esp = setup_main_stack(line, simulated_stack_top);
 // printf("# esp = %08x\n", (unsigned)esp);
  
  /* dump memory area for verification */
  dump(esp, (unsigned)simulated_stack_top - (unsigned)esp);
  
  /* original command-line should not be needed anymore */
  for (i = 0; i < LINE_SIZE; ++i)
    line[i] = (char)0xCC;
  
  /* print the argument vector to see if it worked */
  for (i = 0; i < esp->argc; ++i)
  {
   printf("argv[%d] = %s\n", i, esp->argv[i]);
  }
  printf("argv[%d] = %p\n", i, esp->argv[i]);

  free(simulated_stack);
  
  return 0;
}
