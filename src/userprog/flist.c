#include <stddef.h>

#include "flist.h"
  


void new_map(map_pointer m) { 
  for(int i=0;i<MAX_FD_SIZE;++i) {
	m->content[i] = NULL;
   }
}

int insert(map_pointer m, value_t* f)
{
	if(m == NULL) 
		return -1;
	for(int i=0;i<MAX_FD_SIZE;++i) 	{
		if(m->content[i] == NULL) {
			m->content[i] = f;
			return i;
		}
	}
	return -1;
}

value_t* find(map_pointer m, int key)
{
	return (m == NULL || key < 0 || key >= MAX_FD_SIZE) ? NULL : m->content[key] ;
}

value_t* remove(map_pointer m, int key)
{
	if(m != NULL) {
		value_t* tmp = find(m, key);
		if(tmp != NULL) {
			m->content[key] = NULL;
		}
		filesys_close(tmp);
		return tmp;
	}
	return NULL;
}

void remove_all (map_pointer m) {

	for (int i=0;i<MAX_FD_SIZE;++i) {
		if(m->content[i] != NULL) {
			remove(m, i+2);
		}
	}
}





