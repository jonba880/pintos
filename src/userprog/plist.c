#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <threads/malloc.h>

#include "plist.h"
//#define MSG(message) printf("#PLISTMSG ("message") \n")
#define MSG(...) 
//#define printf(...)
/*
förvirrande variabelnamn:
pid = parent id
palive = parent is alive
*/

struct plist_info *plist_get_process(int process_id) {
  MSG("plist_get_process");
	for (int index=0; index<PLIST_SIZE; index++) {
		 struct plist_info *p = &p_list.data[index];
		if (p->id == process_id) {
			return p;
		}
	}
	return NULL;
}

void plist_set_exit_status(int process_id, int exit_status) {
LOCK;
  	MSG("plist_set_exit");  
	struct plist_info *p = plist_get_process(process_id);
	if (p != NULL)
		p->exit_status = exit_status;
UNLOCK;
}

int plist_get_exit_status(int process_id) {
	MSG("plist_get_exit_status");        
	 struct plist_info *p = plist_get_process(process_id);
        //plist_print(p_list);
	if (p != NULL && !p->removed) {
		sema_down(&p->sema);
		    p->free = true;
    		    p->removed = true;
		return p->exit_status;
	}
	return -1;
}



void plist_init() {
  lock_init(&p_list_lock);
  LOCK;
   MSG("plist_init");   
   for(int index=0; index< PLIST_SIZE; index++) {
    struct plist_info *p =  &p_list.data[index];
    p->free = true;
  }
  UNLOCK;
}


int plist_insert(int process_id, char process_name[], int pid) {
LOCK;
  MSG("plist_insert");
  int return_value = -1;
  for(int index = 0; index< PLIST_SIZE; index++) {
    struct plist_info *p =  &p_list.data[index];
    if (p->free) {
      p->free = false;
      p->alive = true;
      p->removed = false;
      p->exit_status = -1;
      p->id = process_id;
      p->pid = pid;
      struct plist_info * parent = plist_get_process(pid);
      bool parent_is_dead = (parent == NULL) ? true :  !parent->alive;
      p->palive = true;
      strlcpy(p->name, process_name, PLIST_P_NAME);
      sema_init(&p->sema, 0); //
      return_value = index;
      break;
    }
  }
UNLOCK;
  return return_value;
}


void plist_remove(int process_id) {
LOCK;
	
  	MSG("plist_remove");
	struct plist_info *process_to_remove = plist_get_process(process_id);
	if (process_to_remove != NULL) {
           process_to_remove->alive = false;

	  //the process can be removed if parent is dead
	   struct plist_info * parent =  plist_get_process(process_to_remove->pid);
	   bool parent_is_dead = (parent == NULL) ? true :  !parent->alive;
	   process_to_remove->free = parent_is_dead;

	//kill the children of 'process_to_remove'
	int pid = process_to_remove->id;
		MSG("plist_kill_all_children");
	  for(int index=0; index< PLIST_SIZE; index++) {
	    struct plist_info *p =  &p_list.data[index];
	    if (p->pid == pid) {
	      p->palive = false;
	      if (!p->alive) {
		MSG("children are dead");
		p->free = true;
	      }
	    }
	  }
	
	// Räkna upp och väck upp. En mer Ledig resurs
        sema_up(&process_to_remove->sema); //asd
	}
UNLOCK;
}



void plist_print() {
 LOCK;

  printf("# |id   | name                | exits | alive | free  |  pid  | palive |\n");
  for (int index=0; index<PLIST_SIZE; index++) {
    struct plist_info *p = &p_list.data[index];
      if (p->name && strlen(p->name) >= 1) {
	      printf("# |%-5d| %-20s| %-6d| %-6d| %-6d| %-6d| %-7d|\n",
	      p->id,
	      p->name,
	      p->exit_status,
	      p->alive,
	      p->free,
	      p->pid,
	      p->palive
	      );
      }
  }

UNLOCK;  
}


