#ifndef _PLIST_H_
#define _PLIST_H_


#include <threads/synch.h>
#include <stdbool.h>

//trådsynkronisering...oklart
#define LOCK lock_acquire(&p_list_lock)
#define UNLOCK lock_release(&p_list_lock)

#define PLIST_SIZE 512
#define PLIST_P_NAME 64

typedef struct plist_info p_info;

struct plist_info {
 int id,pid, exit_status;
 char name[PLIST_P_NAME];
 bool free, alive, removed,palive;
 struct semaphore sema;
};

struct plist {
 p_info data[PLIST_SIZE];
};

struct lock p_list_lock;
struct plist p_list;


void kill_children(int pid);
void plist_print2();
int plist_insert(int process_id, char *process_name, int pid);
void plist_remove(int process_id);
void plist_print();
void plist_init();
struct plist_info *plist_get_process(int process_id);
void plist_set_exit_status(int process_id, int exit_status);
int plist_get_exit_status(int process_id);



#endif
