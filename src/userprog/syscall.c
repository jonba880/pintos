#include <stdio.h>
#include <syscall-nr.h>
#include "userprog/syscall.h"
#include "threads/interrupt.h"
#include "threads/thread.h"

#include <string.h>
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "threads/vaddr.h"
#include "threads/init.h"
#include "userprog/pagedir.h"
#include "userprog/process.h"
#include "devices/input.h"

#include <stdlib.h>
#include "pagedir.h"
//#include "thread.h"
#define MSG(message) printf("#MSG >SYSCALL ("message") \n")
#define MSG(...) 
#define printf(...) 



//file handeling
void sys_seek (int fd, int position) {
     struct file* file_pointer = find(&thread_current()->fm, (fd-2));
	  if(file_pointer == NULL) {
		  return;
	  }
    file_seek(file_pointer, (off_t)position);
}

unsigned sys_tell (int fd) {
	struct file* file_pointer = find(&thread_current()->fm, (fd-2));
	return file_pointer == NULL ? -1 : file_tell(file_pointer);
		
}

int sys_filesize (int fd) {
	struct file* file_pointer = find(&thread_current()->fm, (fd-2));
	return file_pointer == NULL ? -1 : file_length(file_pointer);
}

int sys_open (const char *file) {
	if (&(thread_current()->fm) == NULL) {
		printf("%s","creating new map.");	
	}
	struct file * file_pointer = filesys_open(file);
	if (file_pointer != NULL) {
		return 2 + insert(&(thread_current()->fm), file_pointer);
	}	
	return -1;
}

bool sys_close (int fd) {
	return remove(&(thread_current()->fm), fd-2);
}

 
bool sys_remove (const char *file) {
	return filesys_remove(file);
}

bool sys_create (const char *file, unsigned initial_size) {
        if(strlen(file) >= MAX_NAME_SIZE || strlen(file) == 0) {
		return 0;
         }
	return filesys_create(file, initial_size);	
}



//tangentbordet identifieras av fil-deskriptor (fd) STDIN_FILENO 
//skärmen identifieras av STDIN_FILENO 

int sys_read(int fd, char *buffer, unsigned length)
{
  if (fd == STDIN_FILENO) {
    char c;
    int i;
    for (i = 0; i < length; ++i) {
      c = input_getc();
      if(c == '\r') {
      	buffer[i] = '\n';
      } else {
	buffer[i] = c;
      }
      putchar(c);
    }		
	return i;
  } else if(fd == STDOUT_FILENO) {
	return -1;
  }  else {
	struct file* f = find(&(thread_current()->fm), fd-2);
	//printf("This is a test, the file id is: %d \n", fd);
	if (f != NULL) {
		off_t red_bytes = file_read(f, (void *)buffer, (off_t) length);
		return red_bytes;
	}
  }
    return -1;	
}


int sys_write(int fd, const char *buffer, unsigned length){
  if (fd == STDOUT_FILENO)
  {
    putbuf(buffer, length);
    return length;
  }  else if(fd == STDIN_FILENO) {
	return -1;
    } else {
	struct file* f = find(&(thread_current()->fm), fd-2);
	//printf("This is a test, the file id is: %d", fd);
	if (f != NULL) {
		off_t written_bytes = file_write(f, (void *)buffer, (off_t) length);
		return written_bytes;
	}
  }
  return -1;
}


/* Verify all addresses from and including 'start' up to but excluding
 * (start+length). */
bool verify_fix_length(char* start, int length)
{

   /* Returns true if VADDR is a user virtual address. */
  if(start == NULL || length < 0 || !is_user_vaddr(start)) {
    return false;
  }
   //. Om pagedir_get_page returnerar NULL är samtliga adresser inom samma sida ogiltiga.
   char*stop = (start+length);
    for (char* cur_adr = pg_round_down(start); cur_adr<stop; cur_adr+=100) {
         if (pagedir_get_page(thread_current()->pagedir, cur_adr) == NULL ) {
		return false;
         }	
    }

  return true;
}

/* Verify all addresses from and including 'start' up to and including
 * the address first containg a null-character ('\0'). (The way
 * C-strings are stored.)
 */
bool verify_variable_length(char* start) {

   /*  is_user_vaddr - Returns true if address is a user virtual address. 
       pagedir_get_page - returns NULL if page is not valid.
       For security reasons we need to make sure an user only can access own memory (stack-based exploitation).
       pg_no(start) != pg_no(start-1) - makes sure we stay within the same page.
      
   */
   if(start == NULL || !is_user_vaddr(start) || pagedir_get_page(thread_current()->pagedir, (start)) == NULL) {
          return false;
   }

  while(*start != '\0') {
    start++;
    if(pg_no(start) != pg_no(start-1) && pagedir_get_page(thread_current()->pagedir, (start)) == NULL) {
          return false;
    }
  }
  return true;
}


static void syscall_handler (struct intr_frame *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}


/* This array defined the number of arguments each syscall expects.
   For example, if you want to find out the number of arguments for
   the read system call you shall write:
   
   int sys_read_arg_count = argc[ SYS_READ ];
   
   All system calls have a name such as SYS_READ defined as an enum
   type, see `lib/syscall-nr.h'. Use them instead of numbers.
 */
const int argc[] = {
  /* basic calls */
  0, 1, 1, 1, 2, 1, 1, 1, 3, 3, 2, 1, 1, 
  /* not implemented */
  2, 1,    1, 1, 2, 1, 1,
  /* extended */
  0
};

static void
syscall_handler (struct intr_frame *f) 
{
  int32_t* esp = (int32_t*)f->esp;
  


  if (f == NULL || esp == NULL) {
	process_exit(-1);
  } 
  


if(!verify_fix_length(&esp[0], 4) || esp[0] > SYS_NUMBER_OF_CALLS || esp[0] < 0){
    process_exit(-1);
}


//addresses to check = 4 addresses per argument + 4 
//verify_fix_length(&esp[0], (argc[esp[0]]+1)*4)  ?! :(






int args_to_syscall = argc[esp[0]] +1;
int n_addr = (args_to_syscall) * 4;  //&esp[5] - &esp[1]
if (!verify_fix_length(&esp[1], n_addr)) {
	process_exit(-1);
}

     
  switch ( esp[0] /* retrive syscall number */ ) {

    case SYS_WAIT:
	MSG ("SYS_WAIT");
	f->eax = process_wait(esp[1]);
    break;	
    case SYS_EXIT:
	MSG ("SYS_EXIT.");
	process_exit(esp[1]);								
    break;
    case SYS_SLEEP:
	MSG ("SYS_SLEEP.");
	timer_msleep (esp[1]); 
    break;	
   case SYS_EXEC:
	MSG ("SYS_EXEC.");
        if(verify_variable_length(esp[1])) {
  	   f->eax = (uint32_t) process_execute((char*) esp[1]);
        } else {
           process_exit(-1);
        }
    break;
    case SYS_PLIST:
	MSG ("SYS_PLIST.");	
	process_print_list();
    break;
    case SYS_HALT:					
	MSG ("SYS_HALT.");	
	power_off();					
    break;						
    case SYS_READ:
          if(verify_fix_length(esp[2], esp[3])) {
                 f->eax = sys_read(esp[1],(char*)esp[2],esp[3]);
	  } else {
		process_exit(-1);
	  }
   	
    break;
    case SYS_WRITE:
         if(verify_fix_length(esp[2], esp[3])) {
	  	f->eax = sys_write(esp[1],(const char*)esp[2],esp[3]); 
	 } else {
       		process_exit(-1);
    	 }
    break;
    case SYS_CREATE:
	 
         if(verify_variable_length(esp[1])) {
              f->eax = sys_create((const char *)esp[1], esp[2]);    
	 } else {
	     process_exit(-1);
         } 
    break;
    case SYS_REMOVE:
       if(verify_variable_length(esp[1])) {
	   f->eax = sys_remove((const char *)esp[1]);
       } else {
           process_exit(-1);
       }
    break;
    case SYS_SEEK:
    	sys_seek(esp[1], esp[2]);
    break;
    case SYS_TELL:
	 f->eax = sys_tell(esp[1]);
    break;
    case SYS_FILESIZE:
	 f->eax = sys_filesize(esp[1]);
    break;
    case SYS_OPEN:
        if(verify_variable_length(esp[1])) {
  	  	f->eax = sys_open((const char*)esp[1]);
  	} else {
		process_exit(-1);
        }
    break;
    case SYS_CLOSE:
	f->eax = sys_close(esp[1]);
    break;
    default:
    {
      printf ("Executed an unknown system call!\n");
      
      printf ("Stack top + 0: %d\n", esp[0]);
      printf ("Stack top + 1: %d\n", esp[1]);
      
      thread_exit ();
    }
  }
}






