#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H
#define MAX_NAME_SIZE 511

void syscall_init (void);

#endif /* userprog/syscall.h */
